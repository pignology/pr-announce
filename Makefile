all: pr-announce

pr-announce: pr-announce.c
	$(CC) -o $@ $< `pkg-config --cflags`

clean:
	rm -f pr-announce.o pr-announce

