/*
** prannounce.c -- sends a UDP broadcast to port 7373 every 5 seconds to pigremote discovery
** (C) Nick Garner, Pignology, LLC, 2014
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#define PINGPORT "7373" 

//Log to omega kernel messages
void log(char *msg) {
  FILE * pFile;
  pFile = fopen ("/dev/kmsg","w");
  fprintf (pFile, "pr-announce:: %s\n", msg);
  fclose (pFile);
}

int main(int argc, char *argv[])
{
    int pinginterval = 5;

    int sockfd;
    struct addrinfo hints, *servinfo, *p;
    int rv;
    int numbytes;

    char dataToSend[] = "pigremote ping";

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;

    if ((rv = getaddrinfo("255.255.255.255", PINGPORT, &hints, &servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        return 1;
    }

    // loop through all the results and make a socket
    for(p = servinfo; p != NULL; p = p->ai_next) {
        if ((sockfd = socket(p->ai_family, p->ai_socktype,
                p->ai_protocol)) == -1) {
            perror("prannounce: socket");
            continue;
        }

        break;
    }

    /* Set socket to allow broadcast */
    int bcastperm = 1;
    if (setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, (void *) &bcastperm, sizeof(bcastperm)) < 0)
    {
        perror("Can't set broadcast permission. Exiting.");
        exit(1);
    }

    if (p == NULL) {
        fprintf(stderr, "prannounce: failed to bind socket\n");
        return 2;
    }

    for (;;)
    {
      if ((numbytes = sendto(sockfd, dataToSend, strlen(dataToSend), 0, p->ai_addr, p->ai_addrlen)) == -1) {
        perror("prannounce: sendto");
        //don't exit if network is down, just try again, DHCP might not have done its job yet
        //exit(1);
      }
      //printf("prannounce: sent %d bytes to 255.255.255.255\n", numbytes);
      sleep(pinginterval);
    }

    freeaddrinfo(servinfo);

    close(sockfd);

    return 0;
}
